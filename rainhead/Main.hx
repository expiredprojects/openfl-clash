package rainhead;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import rainhead.core.GameManager;

class Main extends Sprite 
{
	var inited:Bool;
	
	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, onAdded);
	}
	
	function onAdded(e:Event):Void 
	{
		removeEventListener(Event.ADDED_TO_STAGE, onAdded);
		init();
	}
	
	function init() 
	{
		Lib.current.addChild(new GameManager());
	}
	
	public static function main() 
	{
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
	}
}
