package rainhead.actors;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.geom.Matrix;

class Background extends Sprite
{
	public function new(width:Int, height:Int, bitmapData:BitmapData) 
	{
		super();
		var matrix:Matrix = new Matrix();
		
		graphics.beginBitmapFill(bitmapData, matrix, true);
		graphics.drawRect(0, 0, width, height);
		graphics.endFill();
	}	
}