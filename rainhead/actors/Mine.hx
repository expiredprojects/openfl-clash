package rainhead.actors;

import flash.display.Bitmap;
import flash.display.BitmapData;
import rainhead.data.GameData;
import rainhead.interfaces.IUpdate;

class Mine extends Bitmap implements IUpdate
{
	var pivotX:Int;
	var pivotY:Int;
	var dx:Int;
	var dy:Int;
	var da:Float;
	var dr:Float;
	var ds:Float;
	
	public function new(data:BitmapData, x:Int, y:Int, dx:Int, dy:Int, alpha:Float = 1, da:Float = 0, rotation:Float = 0, dr:Float = 0, scale:Float = 1, ds:Float = 0) 
	{
		super(data);
		this.pivotX = cast(width / 2, Int);
		this.pivotY = cast(height / 2, Int);
		this.x = x - pivotX;
		this.y = y - pivotY;
		this.dx = dx;
		this.dy = dy;
		this.alpha = alpha;
		this.da = da;
		this.rotation = rotation;
		this.dr = dr;
		this.scaleX = scale;
		this.scaleY = scale;
		this.ds = ds;
	}
	
	/* INTERFACE rainhead.interfaces.IUpdate */
	
	public function update():Void 
	{
		modX();
		modY();
	}
	
	function modX():Void
	{
		x -= dx;
		dx = cast(checkBounds(x, 0 - pivotX, GameData.SCREEN_WIDTH - pivotX, dx), Int);
	}
	
	function modY():Void
	{
		y -= dy;
		dy = cast(checkBounds(y, 0 - pivotY, GameData.SCREEN_HEIGHT - pivotY, dy), Int);
	}
	
	function checkBounds(value:Float, left:Float, right:Float, delta:Float):Float 
	{
		if (value - delta <= left)
		{
			return -delta;
		}
		if (value - delta >= right) 
		{
			return -delta;
		}
		else 
		{
			return delta;
		}
	}
}