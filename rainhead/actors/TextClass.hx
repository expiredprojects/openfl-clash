package rainhead.actors;

import flash.text.TextField;
import flash.text.TextFormat;

class TextClass extends TextField
{
	public function new(text:String, x:Int, y:Int) 
	{
		super();
		this.defaultTextFormat = new TextFormat('Times New Roman', 8, 0xdedede);
		this.selectable = false;
		this.text = text;
		this.x = x;
		this.y = y;
	}	
}