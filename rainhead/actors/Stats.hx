package rainhead.actors;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import rainhead.core.GameDispatcher;
import rainhead.core.GameEvent;
import rainhead.data.GameData;

class Stats extends Sprite
{
	var timeText:TextClass;
	var objText:TextClass;
	var frames:Int = 0;
	var date:Date;
	var time:Float;
	var old:Float;
	
	public function new() 
	{
		super();
		timeText = new TextClass("TP60: 0", 2, 1);
		addChild(timeText);
		
		objText = new TextClass("OBJ: 0", 2, 9);
		addChild(objText);
		
		date = Date.now();
		old = date.getTime();
		addEventListener(Event.ENTER_FRAME, onEnterFrame);
		GameDispatcher.instance.addEventListener(GameEvent.UPDATE_EVENT, onUpdate);
	}	
	
	private function onEnterFrame(e:Event):Void 
	{
		if (++frames == 60) 
		{
			date = Date.now();
			time = date.getTime();
			timeText.text = "TP60: " + (time - old)/1000;
			old = time;
			frames  = 0;
		}
	}
	
	private function onUpdate(e:Event):Void 
	{
		objText.text = "OBJ: " + GameData.OBJECTS_NUMBER;
	}
}