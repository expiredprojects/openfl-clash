package rainhead.interfaces;

interface IDisplay 
{
	function addElements(number:Int):Void;
	function removeElements(number:Int):Void;
	function  updateAlpha():Void;
	function  updateRotation():Void;
	function  updateScale():Void;
}