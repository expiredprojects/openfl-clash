package rainhead.core;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.system.System;
import rainhead.actors.Mine;
import rainhead.data.GameData;
import rainhead.interfaces.IDisplay;

class DisplayManager extends Sprite implements IDisplay
{
	var data:BitmapData;
	var mineArray:Array<Mine>;
	
	public function new(data:BitmapData) 
	{
		super();		
		this.data = data;
		mineArray = new Array<Mine>();
		addEventListener(Event.ENTER_FRAME, onEnterFrame);
	}
	
	private function onEnterFrame(e:Event):Void 
	{
		var length:Int = mineArray.length;
		for (i in 0...length) 
		{
			mineArray[i].update();
		}
	}
	
	function randomOne():Int
	{
		return -1 + 2 * cast (Math.random() * 2);
	}
	
	/* INTERFACE rainhead.interfaces.IDisplay */
	
	public function addElements(number:Int):Void 
	{
		var x:Int;
		var y:Int;
		var dx:Int;
		var dy:Int;
		
		for (i in 0...number) 
		{
			x = cast (Math.random() * GameData.SCREEN_WIDTH);
			y = cast (Math.random() * GameData.SCREEN_HEIGHT);
			dx = randomOne();
			dy = randomOne();
			
			mineArray.push(new Mine(data, x, y, dx, dy));
			addChild(mineArray[mineArray.length - 1]);
		}
		
		GameDispatcher.instance.dispatchNumber(mineArray.length);
	}
	
	public function removeElements(number:Int):Void 
	{
		number = mineArray.length - 1 - number;
		var i:Int = mineArray.length - 1;
		
		while (i > number && i > -1) 
		{
			removeChild(mineArray[i]);
			mineArray.splice(i, 1);
			i--;
		}
		
		System.gc();
		GameDispatcher.instance.dispatchNumber(mineArray.length);
	}
	
	public function updateAlpha():Void 
	{
		
	}
	
	public function updateRotation():Void 
	{
		
	}
	
	public function updateScale():Void 
	{
		
	}
}