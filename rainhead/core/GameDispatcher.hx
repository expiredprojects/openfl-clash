package rainhead.core;

import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import rainhead.data.GameData;

class GameDispatcher extends EventDispatcher
{
	static var _instance:GameDispatcher;
	
	public function new() 
	{
		super();		
		_instance = this;
	}
	
	public function dispatchNumber(dataNumber:Int):Void 
	{
		GameData.OBJECTS_NUMBER = dataNumber;
		dispatchEvent(new GameEvent());
	}
	
	public static var instance(get_instance, null):GameDispatcher;
	
	static function get_instance():GameDispatcher 
	{
		return _instance;
	}
}