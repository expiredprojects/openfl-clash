package rainhead.core;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.Lib;
import flash.ui.Keyboard;
import haxe.ds.StringMap;
import openfl.Assets;
import rainhead.actors.Background;
import rainhead.actors.Stats;
import rainhead.data.GameData;

class GameManager extends Sprite
{
	var displayManager:DisplayManager;
	public function new() 
	{
		super();
		addEventListener(Event.ADDED_TO_STAGE, onAdded);
	}
	
	function onAdded(e:Event):Void 
	{
		removeEventListener(Event.ADDED_TO_STAGE, onAdded);
		init();
		stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
	}
	
	function init() 
	{
		var dispatcher:GameDispatcher = new GameDispatcher();
		
		var backgroundData:BitmapData = Assets.getBitmapData("img/cosmos.png");
		var background:Background = new Background(GameData.SCREEN_WIDTH, GameData.SCREEN_HEIGHT, backgroundData);
		addChild(background);
		
		var mineData:BitmapData = Assets.getBitmapData("img/mine_o.png");
		displayManager = new DisplayManager(mineData);
		addChild(displayManager);
		
		var stats:Stats = new Stats();
		addChild(stats);
	}
	
	private function onKeyDown(e:KeyboardEvent):Void 
	{
		switch (e.keyCode) 
		{
			case Keyboard.UP:
				displayManager.addElements(10);
			case Keyboard.DOWN:
				displayManager.removeElements(10);
		}
	}
}