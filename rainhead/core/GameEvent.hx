package rainhead.core;

import flash.events.Event;

class GameEvent extends Event
{
	public static inline var UPDATE_EVENT:String = "update_event";
	
	public function new() 
	{
		super(UPDATE_EVENT, false, false);	
	}
}